import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import wallet.Wallet

import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Dompet Digital")

def common = ExcelKeywords.getExcelSheet(workBook,"Common")

String text = ExcelKeywords.getCellValueByAddress(common, 'D2')

String amountWallet = ExcelKeywords.getCellValueByAddress(sheet1, 'D'+GlobalVariable.rowNumber)

String saveName = ExcelKeywords.getCellValueByAddress(sheet1, 'E'+GlobalVariable.rowNumber)

Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 0)

Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeImage - Pilih Rekening (1)'), 0)

if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 0, FailureHandling.OPTIONAL) == true) {
	
	Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 0)
	
	CustomKeywords.'screenshot.capture.Screenshot'()

	Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 0)
}

else {
	Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

	Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 0)

	CustomKeywords.'screenshot.capture.Screenshot'()

	Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 0)
}

Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), amountWallet, 0)

if (saveName != 'null' && Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0, FailureHandling.OPTIONAL) == true ) {
    Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0)

	Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nama'), saveName, 0)

	Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nomor Tujuan'), 0)
}

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 0)

Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 0)