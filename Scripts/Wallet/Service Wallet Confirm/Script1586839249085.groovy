import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'Dompet Digital')

String wallet = ExcelKeywords.getCellValueByAddress(sheet1, 'A' + GlobalVariable.rowNumber)

String gopayType = ExcelKeywords.getCellValueByAddress(sheet1, 'B' + GlobalVariable.rowNumber)

Mobile.verifyElementExist(findTestObject('Wallet Confirm/XCUIElementTypeButton - Top Up'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//not_run: Mobile.setText(findTestObject('Wallet Confirm/XCUIElementTypeTextField - Kode Promo'), detail.toString(), 0)
//Mobile.tap(findTestObject('Wallet Confirm/XCUIElementTypeStaticText - Nomor Tujuan'), 0)
tax = 'Rp0'

switch (wallet) {
    case 'Gopay':
        if (gopayType.equalsIgnoreCase('Customer')) {
            tax = 'Rp1.000'
        } else if (gopayType.equalsIgnoreCase('Driver')) {
            tax = 'Rp0'
        } else {
            tax = 'Rp0'
        }
        
        break
    case 'OVO':
        tax = 'Rp1.000'

        break
    case 'ShopeePay':
        tax = 'Rp0'

        break
    case 'DANA':
        tax = 'Rp0'

        break
    case 'LinkAja':
        tax = 'Rp0'

        break
    case 'Skip':
        tax = Mobile.getText(findTestObject('Wallet Confirm/XCUIElementTypeStaticText - Admin Wallet'), 0)

        break
    default:
        break
}

text = Mobile.getText(findTestObject('Wallet Confirm/XCUIElementTypeStaticText - Admin Wallet'), 0)

Mobile.comment('Result = ' + text)

Mobile.comment('Expected = ' + tax)

//Mobile.verifyMatch(text, tax, false)
acc = Mobile.getText(findTestObject('Credit Form/XCUIElementTypeStaticText - rekening terpilih'), 0)

Mobile.comment(acc)

GlobalVariable.accountNumber = acc.toString()

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Wallet Confirm/XCUIElementTypeButton - Top Up'), 0)

