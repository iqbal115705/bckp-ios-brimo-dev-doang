import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Catatan Keuangan")

String page = ExcelKeywords.getCellValueByAddress(sheet1, 'A'+GlobalVariable.rowNumber)

Mobile.verifyElementExist(findTestObject('PFM/XCUIElementTypeStaticText - Catatan Keuangan Tittle'), 10)

pages = page

switch(pages) {
	case 'pengeluaran':

		Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - Pengeluaran'), 0)

		break;

	case 'pemasukan':

		Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - Pemasukan'), 0)

		break;

	case 'laporan':

		Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - Laporan'), 0)

		break;

	default :

		Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - Pengeluaran'), 0)

}