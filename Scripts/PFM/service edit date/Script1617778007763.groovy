import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.kms.katalon.core.util.KeywordUtil


dates = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal'), 'value', 0)

Mobile.verifyElementExist(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal', [('date') : "$dates"]), 0)

Mobile.tap(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

dates = date.toString()

Date today = new Date()

String todaysDate = today.format('dd')


def datePlus = today.clone()
def dateMinus = today.clone()

datePlus = datePlus + 1

dateMinus = dateMinus - 1

Mobile.comment("tanggal hari ini " + todaysDate)

switch(dates) {
	case 'today' :
	
		if(todaysDate.charAt(0) == '0') {
			todaysDate = todaysDate.substring(1)
		}
		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeStaticText - tanggal', [('date') : "$todaysDate"]), 0)
		
		CustomKeywords.'screenshot.capture.Screenshot'()

		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeButton - Pilih Tanggal'), 0)
		
		indo_date = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal'), 'value', 0)
		
		GlobalVariable.dateIndo = indo_date
		break;
	case 'tomorrow' :

		String tomorrowDate = datePlus[DATE]
		
		if(tomorrowDate.charAt(0) == '0') {
			tomorrowDate = tomorrowDate.substring(1)
		}

		if(tomorrowDate == '1' || tomorrowDate == '01') {
			KeywordUtil.markPassed('tidak bisa memilih tanggal untuk bulan depan')
			
		} else {
			Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeStaticText - tanggal', [('date') : "$tomorrowDate"]), 0)

			Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeButton - Pilih Tanggal'), 0)
			
			CustomKeywords.'screenshot.capture.Screenshot'()
			
			Mobile.verifyElementNotVisible(findTestObject('PFM/tambah pengeluaran/XCUIElementTypeStaticText - Tambah Catatan'), 0)

		}
		break;
	case 'yesterday' :

		String yesterdayDate = dateMinus[DATE]
		
		if(yesterdayDate.charAt(0) == '0') {
			yesterdayDate = yesterdayDate.substring(1)
		}
		
		if(todaysDate == '1' || todaysDate == '01') {
			Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeStaticText - bulan sebelumnya'), 0)
			
			Mobile.delay(2)
			
			CustomKeywords.'screenshot.capture.Screenshot'()
			
		}

		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeStaticText - tanggal', [('date') : "$yesterdayDate"]), 0)
		
		CustomKeywords.'screenshot.capture.Screenshot'()

		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeButton - Pilih Tanggal'), 0)
		
		indo_date = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal'), 'value', 0)
		
		GlobalVariable.dateIndo = indo_date
		
		break;

	default :
		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeStaticText - tanggal', [('date') : "$todaysDate"]), 0)
		
		CustomKeywords.'screenshot.capture.Screenshot'()

		Mobile.tap(findTestObject('PFM/Pilih Tanggal/XCUIElementTypeButton - Pilih Tanggal'), 0)
		
		indo_date = Mobile.getAttribute(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - edit tanggal'), 'value', 0)
		
		GlobalVariable.dateIndo = indo_date
}

CustomKeywords.'screenshot.capture.Screenshot'()