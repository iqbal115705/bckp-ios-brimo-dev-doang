import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementVisible(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - kategori page edit'), 0)

Mobile.tap(findTestObject('PFM/edit pengeluaran/XCUIElementTypeTextField - kategori page edit'), 0)

//Mobile.verifyElementVisible(findTestObject('PFM/Kategori Pengeluaran/XCUIElementTypeStaticText - Kategori Pengeluaran'), 0)

categories = new_category.toString()

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.70
int endY = device_Height * 0.30

if(Mobile.verifyElementNotVisible(findTestObject('PFM/Kategori Pengeluaran/XCUIElementTypeStaticText - teks kategori', ['cat': "$categories"]), 0, FailureHandling.OPTIONAL) == true) {
	Mobile.swipe(startX, startY, endX, endY)
}

Mobile.tap(findTestObject('PFM/Kategori Pengeluaran/XCUIElementTypeStaticText - teks kategori', ['cat': "$categories"]), 0)

Mobile.delay(1)

CustomKeywords.'screenshot.capture.Screenshot'()