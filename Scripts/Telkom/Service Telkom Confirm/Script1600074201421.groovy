import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection

Mobile.verifyElementExist(findTestObject('Telkom Confirm/XCUIElementTypeOther - Konfirmasi'), 0)

while (Mobile.verifyElementExist(findTestObject('Transfer Confirm/XCUIElementTypeStaticText - Total'), 5) == false) {
    Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
}

Mobile.verifyElementExist(findTestObject('Transfer Confirm/XCUIElementTypeStaticText - Total'), 0)

text = 'Rp0'

tax = Mobile.getText(findTestObject('Telkom Confirm/XCUIElementTypeStaticText - Admin Biaya'), 0)

Mobile.verifyMatch(tax, text, false)

acc = Mobile.getText(findTestObject('Credit Form/XCUIElementTypeStaticText - rekening terpilih'), 0)

Mobile.comment(acc)

GlobalVariable.accountNumber = acc.toString()

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Telkom Confirm/XCUIElementTypeButton - Bayar'), 0)

