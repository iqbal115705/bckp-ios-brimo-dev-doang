import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'Listrik')

debit = GlobalVariable.bankAcc.toString()

String amountListrik = ExcelKeywords.getCellValueByAddress(sheet1, 'C' + GlobalVariable.rowNumber)

String saveName = ExcelKeywords.getCellValueByAddress(sheet1, 'D' + GlobalVariable.rowNumber)

String whatPayment = ""

if (Mobile.verifyElementExist(findTestObject('Object Repository/Listrik Nominal Close/XCUIElementTypeOther - Tagihan'), 
    10, FailureHandling.OPTIONAL) == true) {
    whatPayment = 'Tagihan'
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/Listrik Nominal Open/XCUIElementTypeOther - Listrik'), 
    10, FailureHandling.OPTIONAL) == true) {
    whatPayment = 'Token'
}

Mobile.comment(whatPayment)

switch (whatPayment) {
    case 'Tagihan':
        Mobile.verifyElementExist(findTestObject('Object Repository/Listrik Nominal Close/XCUIElementTypeOther - Tagihan'), 
            0)

        Mobile.tap(findTestObject('Listrik Nominal Close/XCUIElementTypeStaticText - Total Bayar'), 0)

        Mobile.tap(findTestObject('Object Repository/Listrik Nominal Close/XCUIElementTypeImage - Pilih Rekening'), 0)

        text = debit.toString()

        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$text"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$text"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
                0)
        } else {
            Mobile.delay(5)

            Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$text"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
                0)
        }
        
        if ((saveName != 'null') && (Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 
            0, FailureHandling.OPTIONAL) == true)) {
            Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0)

            Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nama'), saveName, 0)

            Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - nominal total bayar'), 0)
        }
        
        CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Object Repository/Listrik Nominal Close/XCUIElementTypeButton - Bayar'), 0)

        break
    case 'Token':
        Mobile.verifyElementExist(findTestObject('Object Repository/Listrik Nominal Open/XCUIElementTypeOther - Listrik'), 
            0)

        Mobile.tap(findTestObject('Object Repository/Listrik Nominal Open/XCUIElementTypeImage - Pilih Rekening'), 0)

        text = debit.toString()

        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$text"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$text"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
                0)
        } else {
            Mobile.delay(5)

            Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$text"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
                0)
        }
        
        if ((saveName != 'null') && (Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 
            0, FailureHandling.OPTIONAL) == true)) {
            Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0)

            Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nama'), saveName, 0)

            Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nomor Tujuan'), 0)
        }
        
        Mobile.delay(2)

        CustomKeywords.'operation.features.clickAmountPulsa'(amountListrik.toString())

        CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Object Repository/Listrik Nominal Open/XCUIElementTypeButton - Beli'), 0)

        break
}