import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection

Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pembelian Terakhir'), 0)

Mobile.setText(findTestObject('Pulsa Form/XCUIElementTypeTextField - No Handphone'), numberPhone.toString(), 0)

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pembelian Terakhir'), 0)

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeButton - Pulsa'), 0)

//Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pulsa'), 0)
//Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
//Mobile.comment(' ini amount '+ amountPulsa.toString())
CustomKeywords.'operation.features.clickAmountPulsa'(amountPulsa.toString())

//CustomKeywords.'option.amount.clickOptionPulsa'(amountPulsa.toString())
//amount = amountPulsa.toString()
//
//trimAmount = amount.replaceAll("\\D+", "")
//
//nominal = 'Rp'+amount+'.000'
//
//Mobile.comment('==='+nominal+'===')
//
//Mobile.tap(findTestObject('Pulsa Form/simpati/XCUIElementTypeStaticText - pulsa dinamis', ['nominal': "$nominal"]), 0)
Mobile.delay(3)

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeButton - Beli'), 0)

