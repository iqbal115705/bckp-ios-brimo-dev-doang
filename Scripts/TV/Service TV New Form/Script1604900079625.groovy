import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'TV')

String tvType = ExcelKeywords.getCellValueByAddress(sheet1, 'A' + GlobalVariable.rowNumber)

String tvNumber = ExcelKeywords.getCellValueByAddress(sheet1, 'B' + GlobalVariable.rowNumber)

//Mobile.verifyElementExist(findTestObject('TV New Form/XCUIElementTypeStaticText - Bayar TV'), 0)
Mobile.tapAndHold(findTestObject('TV New Form/XCUIElementTypeTextField - Pilih Jenis Layanan'), 0, 0)

String value = "Bayar TV"

switch (tvType) {
    case 'Indosat':
        if (Mobile.verifyElementExist(findTestObject('TV New Form/XCUIElementTypeStaticText - Indosat GIG'), 5, FailureHandling.OPTIONAL) == true) {
            Mobile.tap(findTestObject('TV New Form/XCUIElementTypeStaticText - Indosat GIG'), 0)
        } else {
            Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : '$value']), 0)
        }
        
        break
    case 'MyRepublic':
        if (Mobile.verifyElementExist(findTestObject('TV New Form/XCUIElementTypeStaticText - MyRepublic Retail'), 5, FailureHandling.OPTIONAL) == true) {
            Mobile.tap(findTestObject('TV New Form/XCUIElementTypeStaticText - MyRepublic Retail'), 0)
        } else {
            Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : '$value']), 0)
        }
        
        break
    case 'Transvision':
        if (Mobile.verifyElementExist(findTestObject('TV New Form/XCUIElementTypeStaticText - Transvision'), 5, FailureHandling.OPTIONAL) == true) {
            Mobile.tap(findTestObject('TV New Form/XCUIElementTypeStaticText - Transvision'), 0)
        } else {
            Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : '$value']), 0)
        }
        
        break
    default:
        break
}

Mobile.setText(findTestObject('TV New Form/XCUIElementTypeTextField - Nomor Pelanggan'), tvNumber, 0)

Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : '$value']), 
    0)

Mobile.tap(findTestObject('TV New Form/XCUIElementTypeButton - Lanjutkan'), 0)