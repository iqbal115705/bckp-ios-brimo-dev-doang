import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

String whatPayment = ""

Mobile.delay(3)

if (Mobile.verifyElementExist(findTestObject('Credit Nominal Open/XCUIElementTypeStaticText - Total Pembayaran'), 5, FailureHandling.OPTIONAL) == 
true) {
    whatPayment = 'Open'
} else if (Mobile.verifyElementExist(findTestObject('Credit Nominal Close/XCUIElementTypeOther - Kartu Kredit'), 5, FailureHandling.OPTIONAL) == 
true) {
    whatPayment = 'Close'
} else {
}

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'Kartu Kredit')

String totalInput = ExcelKeywords.getCellValueByAddress(sheet1, 'E' + GlobalVariable.rowNumber)

String amountCredit = ExcelKeywords.getCellValueByAddress(sheet1, 'C' + GlobalVariable.rowNumber)

def common = ExcelKeywords.getExcelSheet(workBook,"Common")

String debit = ExcelKeywords.getCellValueByAddress(common, 'D2')

String saveName = ExcelKeywords.getCellValueByAddress(sheet1, 'D' + GlobalVariable.rowNumber)

heigth = Mobile.getDeviceHeight()

width = Mobile.getDeviceWidth()

int startX = width * 0.50

int endX = startX

int startY = heigth * 0.70

int endY = heigth * 0.30

switch (whatPayment) {
    case 'Open':
        Mobile.verifyElementExist(findTestObject('Credit Nominal Open/XCUIElementTypeStaticText - Total Pembayaran'), 0)

        switch (totalInput) {
            case 'Penuh':
                Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeButton - Bayar Penuh'), 0)

                break
            case 'Minimal':
                Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeButton - Bayar Minimal'), 0)

                break
            case 'Manual':
                Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeButton - Input Nominal'), 0)

                text = '0'

                Mobile.setText(findTestObject('Credit Nominal Open/XCUIElementTypeTextField - Nominal', [('text') : "$text"]), 
                    amountCredit, 0)

                Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeStaticText - Nominal Pembayaran'), 0)

                break
            default:
                break
        }
        
        //        while (Mobile.verifyElementExist(findTestObject('Credit Nominal Open/XCUIElementTypeStaticText - Sumber Dana'), 
        //            5) == false) {
        //            Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
        //        }
        //			
        Mobile.swipe(startX, startY, endX, endY)

        //Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
        Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeImage - Pilih Rekening'), 0)

        //Mobile.delay(180)
        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$debit"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0 //Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
                )
        } else {
            Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

            Mobile.swipe(startX, startY, endX, endY)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        }
        
        CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Credit Nominal Open/XCUIElementTypeButton - Bayar'), 0)

        break
    case 'Close':
        Mobile.verifyElementExist(findTestObject('Credit Nominal Close/XCUIElementTypeOther - Kartu Kredit'), 0)

        text = '0'

        Mobile.setText(findTestObject('Credit Nominal Close/XCUIElementTypeTextField - Nominal', [('text') : "$text"]), 
            amountCredit, 0)

        Mobile.tap(findTestObject('Credit Nominal Close/XCUIElementTypeStaticText - Nominal Pembayaran'), 0)

        Mobile.tap(findTestObject('Credit Nominal Close/XCUIElementTypeImage - Pilih Rekening'), 0)

        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$debit"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0 //Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
                )
        } else {
            Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

            Mobile.swipe(startX, startY, endX, endY)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        }
        
		if ((saveName != 'null') && (Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'),
			0, FailureHandling.OPTIONAL) == true)) {
			Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0)

			Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nama'), saveName, 0)

			String value = 'Nomor Tujuan'
			
			Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : "$value"]), 0)
		}
		
        CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Credit Nominal Close/XCUIElementTypeButton - Bayar'), 0)

        break
    default:
        break
}
