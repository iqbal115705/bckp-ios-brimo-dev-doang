import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.testobject.TextField as TextField
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.MobileElement as MobileElement

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'Asuransi')

String insuranceType = ExcelKeywords.getCellValueByAddress(sheet1, 'A' + GlobalVariable.rowNumber)

String paymentType = ExcelKeywords.getCellValueByAddress(sheet1, 'B' + GlobalVariable.rowNumber)

String insuranceNumber = ExcelKeywords.getCellValueByAddress(sheet1, 'C' + GlobalVariable.rowNumber)

Mobile.delay(2)

//Mobile.tap(findTestObject('Asuransi New Form/XCUIElementTypeTextField - Pilih Jenis Asuransi'), 0)
Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown pilih asuransi'), 0)

switch (insuranceType) {
    case 'BRINS':
        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - BRI Insurance (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - BRI Insurance'), 0)

        break
    case 'Prudential':
        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Prudential (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Prudential'), 0)

//        Mobile.delay(2)

        not_run: Mobile.tapAndHoldAtPosition(250, 408, 2)

        Mobile.setText(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Nomor Tujuan'), '', 
            0)

        Mobile.tapAndHoldAtPosition(250, 408, 2)

        break
    default:
        break
}

//Mobile.delay(2)

not_run: Mobile.tapAndHoldAtPosition(250, 408, 2)

Mobile.setText(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Nomor Tujuan'), '', 0)

Mobile.tapAndHoldAtPosition(250, 408, 2)

//Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)
//Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 0)
switch (paymentType) {
    case 'Premi Pertama':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - premi pertama spaj'), 
            0)

        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - premi pertama (SPAJ)'), 0)
        break
    case 'Premi Lanjutan':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Premi Lanjutan (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Premi Lanjutan'), 0)

        break
    case 'Top Up Premi':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Pilih Tipe Pembayaran'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Top Up Premi (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Top Up Premi'), 0)

        break
    case 'Biaya Cetak Ulang Polis':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Pilih Tipe Pembayaran'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        //Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Cetak Ulang Polis (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Cetak Ulang Polis'), 
            0)

        break
    case 'Biaya Perubahan Polis':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Pilih Tipe Pembayaran'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Perubahan Polis (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Perubahan Polis'), 
            0)

        break
    case 'Biaya Cetak Kartu':
        Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Button Pilih Jenis Gopay'), 0, FailureHandling.STOP_ON_FAILURE)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Pilih Tipe Pembayaran'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - dropdown tipe asuransi'), 
            0)

        //        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Cetak Kartu (1)'), 0)
        Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Biaya Cetak Kartu'), 
            0)

        break
    default:
        break
}

Mobile.setText(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeTextField - Nomor Tujuan'), insuranceNumber, 0)

Mobile.delay(3)

Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeStaticText - Bayar Asuransi'), 0)

Mobile.tap(findTestObject('Object Repository/Asuransi New Form/XCUIElementTypeButton - Lanjutkan'), 0)

