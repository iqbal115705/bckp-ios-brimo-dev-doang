import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.inspect.swingui.BytecodeCollector
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory

import com.kms.katalon.core.model.FailureHandling as FailureHandling

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import io.appium.java_client.MobileElement
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Pulsa")

String numberPhone = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

Mobile.comment(numberPhone)

String amountPulsa = ExcelKeywords.getCellValueByAddress(sheet1, 'D'+GlobalVariable.rowNumber)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pembelian Terakhir'), 0)

Mobile.setText(findTestObject('Pulsa Form/XCUIElementTypeTextField - No Handphone'), numberPhone, 0)

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pembelian Terakhir'), 0)

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeButton - Paket Data'), 0)

height = Mobile.getDeviceHeight()

width = Mobile.getDeviceWidth()

int startX = width*0.50

int endX = startX

int startY = height*0.80

int endY = height*0.20

Mobile.swipe(startX, startY, endX, endY)

//Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Pulsa'), 0)
//Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

//Mobile.comment(' ini amount '+ amountPulsa.toString())

//CustomKeywords.'operation.features.clickAmountPulsa'(amountPulsa.toString())

//CustomKeywords.'option.amount.clickOptionPulsa'(amountPulsa.toString())

amount = amountPulsa

trimAmount = amount.replaceAll("\\D+", "")

nominal = 'Rp'+amount+'.000'

Mobile.comment('==='+nominal+'===')

AppiumDriver<?> driver = MobileDriverFactory.getDriver()

List<MobileElement> elements = driver.findElementsByName(nominal)

println('size element'+ elements.size())

for(int i = 0; i<=elements.size(); i++) {
	String act_text = elements[i].getAttribute('index')
	
	if(act_text.equalsIgnoreCase('true')) {
		act_text = 1
	} else if(act_text.equalsIgnoreCase('false')){
		act_text = 0
	}
	
	//println('ini index halaman '+ act_text)
	
	if(act_text == '1') {
		elements[i].click()
		
		break;
	}
}

Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeButton - Beli'), 0)

