import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('General/Database Connect'), [:], FailureHandling.STOP_ON_FAILURE)

CustomKeywords.'database.methods.executeUpdate'(('DELETE FROM tbl_user_account WHERE username = "' + username.toString()) + '"')

Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 10)

if(funds.toString() == '1' || funds.toString() == 'saving') {
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','001901001364305','SA','BritAma','Buanyak','IDR','6013010000000342',1,1,1,'TA')"))
	
	Mobile.delay(10)
	
	Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 10)
} else if(funds.toString() == '5' || funds.toString() == 'giro'){
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','001901001364305','SA','Britama Digital','orang kaya saving','IDR','5221845010628089',1,1,0,'TA')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','020601000064301','CA','Britama Digital','orang kaya giro','IDR','5221849000000253',1,1,0,'GB')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','020601000051308','SA','Britama Digital','orang menengah saving','IDR','5221849000000253',1,1,0,'TA')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','020601002751302','CA','BritAma','giro iqbal','IDR','5221849000000253',1,1,1,'TA')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','032901039026504','SA','Britama Digital','lower ibas','IDR','5221847200000019',1,1,0,'TA')"))
	
	Mobile.delay(10)
	
	Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 10)
} else if(funds.toString() == '4') {
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','001901015545503','SA','Britama Digital','freeze','IDR','5221845010628089',1,1,0,'TA')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','001901000004538','SA','Simpedes','dormant','IDR','5221849000000253',1,1,0,'SU')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','020601000048559','SA','Britama Digital','closed','IDR','5221849000000253',1,1,0,'BZ')"))
	CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('" + username.toString() + "','020601002751302','CA','BritAma','giro iqbal','IDR','5221849000000253',1,1,1,'TA')"))
	
	Mobile.delay(10)
	
	Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 10)
} else if(funds.toString() == 'suspend'){
	 CustomKeywords.'database.methods.executeUpdate'(("INSERT INTO tbl_user_account (id,username,account,type_account,product_type,account_name,currency,card_number,status,finansial_status,`default`,sc_code) VALUES ('11111','" + username.toString() + "','020601000136506','SA','BritAma','Buanyak','IDR','6013010000000342',1,1,1,'TA')"))
	 
	 Mobile.delay(10)
	 
	 Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 10)
}

Mobile.delay(10)

Mobile.tap(findTestObject('Wallet New Form/XCUIElementTypeButton - Back'), 0)

Mobile.delay(3)

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.30
int endY = device_Height * 0.70

Mobile.swipe(startX, startY, endX, endY)

Mobile.delay(3)

if(Mobile.verifyElementExist(findTestObject('/General/XCUIElementTypeButton - BrimoEyeOpen'), 10, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('/General/XCUIElementTypeButton - BrimoEyeOpen'), 0)
} else {
	Mobile.tap(findTestObject('/General/XCUIElementTypeButton - BrimoEyeClose'), 0)
}

Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Lainnya'), 0)

Mobile.verifyElementExist(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Dompet Digital'), 0)

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Wallet Form/XCUIElementTypeCell - Save 1'), 0)

WebUI.callTestCase(findTestCase('General/Database Close'), [:], FailureHandling.STOP_ON_FAILURE)
