import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.verifyElementExist(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 0)

accountFund = Mobile.getText(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - nomor rekening terpilih'), 0)

GlobalVariable.accountNumber = accountFund

text = Mobile.getText(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Detail Nomor Tujuan'), 0)

//Mobile.comment(text)

String WalletKept = text.substring( 0, text.indexOf(" -"));

//Mobile.comment(WalletKept)

String walletSelected

String walletType

if (WalletKept.contains("GoPay") == true) {
	
	 walletType = WalletKept.substring(WalletKept.indexOf("(")+1, WalletKept.indexOf(")"));
	 
	 walletSelected = text.substring( 0, text.indexOf(" ("));
	
	 //Mobile.comment(walletSelected)
	 
	 GlobalVariable.walletOption = walletSelected
	 
	 //Mobile.comment(walletType)
	 
	 GlobalVariable.typeGopay = walletType
} else {
	//Mobile.comment(WalletKept)
	
	GlobalVariable.walletOption = WalletKept
}

Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nominal'), walletAmount.toString(), 0)

Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - Nominal'), 0)

nominalText = Mobile.getText(findTestObject('Wallet Nominal Form/XCUIElementTypeStaticText - nominal pada inquiry'), 0)

if(nominalText.contains('-') == true) {
	Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeImage - arrow down sumber dana'), 0)
	
	Mobile.delay(10)
	
	Mobile.tapAtPosition(200, 500)
}

Mobile.verifyElementNotExist(findTestObject('Object Repository/Wallet Nominal Form/XCUIElementTypeStaticText - Saldo Anda tidak mencukupi'), 15)

//Mobile.comment(GlobalVariable.walletOption)

//Mobile.comment(GlobalVariable.typeGopay)

Mobile.verifyElementAttributeValue(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Top Up'), 'enabled', 'true', 0)
	
CustomKeywords.'screenshot.capture.Screenshot'()

