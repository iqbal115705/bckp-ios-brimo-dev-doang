import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.detroitlabs.katalonmobileutil.touch.Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.keyword.excel.ExcelKeywords

import internal.GlobalVariable

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def common = ExcelKeywords.getExcelSheet(workBook,"Common")

String text = ExcelKeywords.getCellValueByAddress(common, 'D2')

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Tarik Tunai")

String amountWithdraw = ExcelKeywords.getCellValueByAddress(sheet1, 'A'+GlobalVariable.rowNumber)

Mobile.verifyElementExist(findTestObject('Tarik Tunai Form/XCUIElementTypeOther - Tarik Tunai'), 0)

Mobile.verifyElementExist(findTestObject('Tarik Tunai Form/XCUIElementTypeStaticText - Sumber Dana'), 0)

Mobile.tap(findTestObject('Tarik Tunai Form/XCUIElementTypeImage - Pilih Rekening'), 0)

if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
    0, FailureHandling.OPTIONAL) == true) {
    Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)

    Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)
} else {
    Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

    Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)

    Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)
}

Mobile.verifyElementExist(findTestObject('Tarik Tunai Form/XCUIElementTypeStaticText - Sumber Dana'), 0)

//CustomKeywords.'operation.features.clickAmountPulsa'(amountWithdraw)

switch (amountWithdraw) {
	case '10000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp10.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp10.000'), 0)

		break
	case '15000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp15.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.comment(' ini amount kondisi'+ amountWithdraw)

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp15.000'), 0)

		break
	case '20000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp20.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp20.000'), 0)

		break
	case '25000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp25.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.comment(' ini amount kondisi'+ amountWithdraw)

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp25.000'), 0)

		break
	case '30000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp30.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp30.000'), 0)

		break
	case '40000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp40.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp40.000'), 0)

		break
	case '50000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp50.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp50.000'), 0)

		break
	case '75000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp75.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp75.000'), 0)

		break
	case '100000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp100.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp100.000'), 0)

		break
	case '150000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp150.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp150.000'), 0)

		break
	case '200000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp200.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp200.000'), 0)

		break
	case '300000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp300.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp300.000'), 0)

		break
	case '500000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp500.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp500.000'), 0)

		break
	case '1000000':
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp1.000.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp1.000.000'), 0)

		break
	default:
		while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp100.000'), 0, FailureHandling.OPTIONAL) == false) {
			Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
		}

		Mobile.comment(' ini amount kondisi default'+ amountWithdraw)

		Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp100.000'), 0)
		break
}

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Tarik Tunai Form/XCUIElementTypeButton - Lanjutkan'), 0)

