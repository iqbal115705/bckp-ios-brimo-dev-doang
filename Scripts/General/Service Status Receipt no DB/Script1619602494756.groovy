import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import export.WriteTxt as WriteTxt

Mobile.waitForElementPresent(findTestObject('Status Receipt/Old/XCUIElementTypeStaticText - Transaksi Berhasil'), 0)

Mobile.verifyElementExist(findTestObject('Status Receipt/Old/XCUIElementTypeStaticText - Transaksi Berhasil'), 0)

//String refnum_receipt
//
//switch (GlobalVariable.deviceHeight) {
//    case '667':
//        refnum_receipt = Mobile.getText(findTestObject('Status Receipt/XCUIElementTypeStaticText - Ip6 Refnum Transaksi'),
//            0)
//
//        break
//    case '812':
//        refnum_receipt = Mobile.getText(findTestObject('Status Receipt/Old/XCUIElementTypeStaticText - IpX Refnum Transaksi'),
//            0)
//
//        break
//    default:
//        break
//}
//refnum_receipt = Mobile.getText(findTestObject('Status Receipt/XCUIElementTypeStaticText - Refnum'), 0)

GlobalVariable.reportStatus = 'success'

if(Mobile.verifyElementVisible(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Biaya Admin Nominal Dinamis'), 5, FailureHandling.OPTIONAL) == true) {
	
	text = Mobile.getText(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - Biaya Admin Nominal Dinamis'), 0)
	
	dateTime = Mobile.getText(findTestObject('Object Repository/General/Bukti Transaksi/waktu dan tanggal'), 0)
	
	refnum = Mobile.getText(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - refNum'), 0)
	
	issuer = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - nama bank dinamis'), 0)
	
	fitur = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - jenis transaksi dinamis'), 0) +' '+ issuer
	
	nominal = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - nominal dinamis'), 0)
	
	CustomKeywords.'export.WriteTxt.fileWrite'(fitur, dateTime, refnum, nominal, text)
	
} else {
	
	text = 'null'
	
	dateTime = Mobile.getText(findTestObject('Object Repository/General/Bukti Transaksi/waktu dan tanggal'), 0)
	
	refnum = Mobile.getText(findTestObject('General/Bukti Transaksi/XCUIElementTypeStaticText - refNum'), 0)
	
	issuer = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - nama bank dinamis'), 0)
	
	fitur = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - jenis transaksi dinamis'), 0) +' '+ issuer
	
	nominal = Mobile.getText(findTestObject('Credit Confirm/XCUIElementTypeStaticText - nominal dinamis'), 0)
	
	CustomKeywords.'export.WriteTxt.fileWrite'(fitur, dateTime, refnum, nominal, text)
	
}

//Mobile.comment(refnum_receipt)

Mobile.delay(3)

CustomKeywords.'screenshot.capture.Screenshot'()

Mobile.tap(findTestObject('Status Receipt/Old/XCUIElementTypeButton - OK'), 0)

