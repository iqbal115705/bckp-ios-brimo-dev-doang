import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
//import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
//import io.appium.java_client.AppiumDriver
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def common = ExcelKeywords.getExcelSheet(workBook, 'Common')

String identifierApp = ExcelKeywords.getCellValueByAddress(common, 'E2')

String AccBank = ExcelKeywords.getCellValueByAddress(common, 'D2')

//Mobile.comment(identifierApp)
Mobile.startExistingApplication(identifierApp, FailureHandling.STOP_ON_FAILURE)

//AppiumDriver driver = MobileDriverFactory.getDriver();
//driver.resetApp()
Mobile.verifyElementExist(findTestObject('Fast Menu/XCUIElementTypeButton - Login'), 0)

GlobalVariable.bankAcc = AccBank