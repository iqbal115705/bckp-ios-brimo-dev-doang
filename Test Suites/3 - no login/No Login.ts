<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>No Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2f5b5c7c-e46a-4456-9a7d-b1560ab14871</testSuiteGuid>
   <testCaseLink>
      <guid>ce083381-2cd0-46c6-8fd8-41b1b7d39cb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Saldo/Info Saldo Awal No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05da0a93-457e-409c-b2c0-640fb7ef695c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/KAI/KAI No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d309d6f0-80e2-4bdd-84f9-61b099efdb7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Briva/BRIVA No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf929b02-8a3a-499c-be1f-d7d201cb436a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/BRINS No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3351b432-6f85-46b9-8151-3fbd1a157a59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Premi Awal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>485a0ba8-76cc-4324-856a-9d27ead9fd5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Premi Lanjut</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ded12c07-32c4-4adc-b667-5d5ebb1b45c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Top Up Premi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46f81c88-d210-47f3-8117-6f3e0ae026d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Cetak ulang polis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07b1443b-85b9-4b68-b3d7-2f330ad196c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Perubahan polis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1412e2ac-3b67-470c-8c28-b5b86e39d57b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential No Login - Cetak Kartu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c12ed71-0718-43e2-9143-f326a092b19a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Brizzi/Brizzi No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3656a3bc-3502-4aa5-be08-81a43b06b1c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Cicilan/OTO No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b628aff2-1484-47f1-98cd-91eedf36e48b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Cicilan/WOM No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db02c638-81ec-4d83-a481-37c10f5770c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/BRI no login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b04d823-7e18-4e2c-bd22-247bd359cab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/Citibank No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04d96c52-fa58-4fe7-9124-0176e31d7729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/DBS No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9617315-1fa1-40f8-b7a4-3037f00c9d5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/HSBC No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5acdac22-394c-4da6-9e10-acbfb48d6fc2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/Standard Chartered No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6c71f69-ccbf-45fa-a623-6b296ffc9c69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Detail Promo/Promo No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8393c59-d76e-4f1e-9681-f69f96e3be17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/Dompet Dhuafa Infaq No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdcfd91f-0c3b-455d-b895-b82cff33bae8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/Dompet Dhuafa Zakat No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35d42ec8-ff43-4503-9ed3-f4764c4150c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/YBM BRI Infaq No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35301504-c03b-4215-8318-5e7faf8361e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/YBM BRI Zakat No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7204b2e0-93c6-495a-a2fd-83e30b450dfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/DPLK/dplk no login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e756e73b-3463-4838-9ce4-48cde0d2772f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Kode QR/Kode QR No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86ec331f-b1ee-43da-9a35-ac0a16d034d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Listrik/Tagihan No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f54fafea-66ee-49bb-9c86-f0bf93b69742</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Listrik/Token No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8fe07c7-24cd-48c7-bcd2-d6454efca423</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pascabayar/pascabayar No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9f2cbd2-fb94-440a-b3d9-381880db5721</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Artarjasa Mentari IM3 No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e41bdb22-a692-4338-94ce-b3f6032d7a78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Axis No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0691de9e-8a80-4a9e-b51f-419b35fc0f7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Finnet Telkomsel No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c36f4523-a6cf-4812-87b3-b7ca7c1b7d57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Mitrakom XL No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7455074f-34fd-4eca-9312-ce3b113eeee9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Smartfren No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3c74a70-dd72-4b6a-9104-9ef305ed1a7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Three No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e06dfede-8342-4a5a-be2d-4bcf7c16a0c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa Data/Indosat No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccf1923a-2fb0-4716-b162-9510ea93b563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa Data/Telkomsel No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e81c445-1af2-40bf-867f-deca983bcf16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Tarik Tunai/Tarik tunai No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4ad8b27-503e-4614-b202-5e0643464c44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Telkom/telkom No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcd20786-cf35-4068-aeed-920384448e11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BRI No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24a9ca36-7fcf-4b35-a94b-dfe5b30f26b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer MANDIRI No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>485a951b-984d-4b1e-a5bd-ed74d3face41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BCA No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4e90060-c357-47b6-91c4-fc5e7dc80a05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BNI No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb2c104b-0d81-4040-9fa0-4f1e1df42689</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer CITIBANK No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>063d7e8d-fe7c-470e-9232-a4faa9132d8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/indosat GIG No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5630d504-2ba7-4c5f-ad95-81ebb6223ad7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/Transvision No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a73878f7-2e51-4219-8984-ad35427bfdee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/My Republic No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67f04146-fd07-4142-b681-9877b4893e61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Dana No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86970256-95c3-4dc7-99e3-41b4b7862e52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/GoPay Customer No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d4d4583-fcfa-4261-9d7d-9b17ce1844a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/GoPay Driver No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b25bbd4f-414a-4e1f-933c-a056d27203e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/LinkAja No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5d728c4-f34b-4f1a-812e-6548c972a12e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Ovo No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d819859-f896-4901-8601-a37370470257</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/ShopeePay No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4da09cc2-6e28-4992-bd7c-34a1d39a3fbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/PFM/Add Catatan Pemasukan No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>650ee28c-1d4a-4fc4-84ad-cbc45ebf5867</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/PFM/Add Catatan Pengeluaran No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d22baba0-047c-4f51-ad51-4c4c3a9b60f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/PFM/Add Catatan Pemasukan Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe7bf50c-1fa0-498d-9f42-e8946a4b79c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/PFM/Add Catatan Pengeluaran Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a329311f-0848-4a75-8ce6-ee683c8caeb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08fecb4d-3c22-468d-b1ed-c4890499cc38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ac077a-1a35-401f-a6f1-4dc5439f6284</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List OVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71612c66-1eb3-4e9c-84d5-c064b8bbd820</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aea98e55-9534-42d6-82ae-77b387b3fde1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2862f12e-d2cc-4a7a-8657-32ee82fbf3d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa Data/Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95d350a7-ca0f-4af5-a217-472ef28eabf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa Data/Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>026bfb70-571e-4bfb-a48b-44963b5f6b12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Kode QR/Kode QR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d5a7aec-f5b9-4bf4-94e6-9f113adcee37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/BRIZZI/Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34fe40b1-d3f3-48fb-b5bc-3eeb3fdc47cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Three</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d93cd785-c7b2-4fab-abc6-5bf65950a44b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d916f78b-5df8-4321-bc55-7382c101b5e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e086e5a-a1f7-492a-a7ba-8fef6e010b0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d473d213-2d65-410f-8e73-59e0999b194c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e18de7ed-f720-42c6-9107-7f6baa78c50a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9eb4d020-0c30-4e9e-aa85-dbeb7b619c0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Promo/Detail Promo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
