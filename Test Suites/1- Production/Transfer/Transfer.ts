<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Transfer</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0cf514d3-57f2-4b9b-8b7e-bbae32d7804f</testSuiteGuid>
   <testCaseLink>
      <guid>ee629769-0b88-4d25-8f91-4226365af132</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a31e9fc4-340b-45d5-9aad-2bcb81f070b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer MANDIRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>847f5874-7e98-4ecc-8c43-b12e8705bc99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>442c1bd7-102c-4f98-b495-af814e48a478</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2da4d75-ee1a-4e3b-82e2-b0923e734131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer CITIBANK</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
