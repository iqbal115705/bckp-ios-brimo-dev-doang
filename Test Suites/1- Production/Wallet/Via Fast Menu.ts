<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Via Fast Menu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5a269bae-db0d-443a-952d-167232d3f7ec</testSuiteGuid>
   <testCaseLink>
      <guid>b84a664a-bb81-42ab-bbd9-d413c4676d8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Via Fast Menu List ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49c8ca8e-9ae8-40af-b6c0-f9310b5ce74c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Via Fast Menu List OVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7bf2099-68f0-464a-9add-df23f9204b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Via Fast Menu List LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2590750e-833c-4457-b47a-fec2ddc00b8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Via Fast Menu List Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63926eea-5540-4091-9b49-5938a52a448e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Via Fast Menu List Dana</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
