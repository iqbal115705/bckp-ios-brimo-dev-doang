<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Use Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>15</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3cc91a8f-85ae-40fb-bddd-e3082ad01c70</testSuiteGuid>
   <testCaseLink>
      <guid>38eb6b6d-e07e-4ae9-85b8-01c6b8a001be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Saldo/Info Saldo Awal No Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22613e9e-8992-4c15-a906-d34402ea91aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/BRINS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7eabf459-18d0-4347-b6bc-41833b1c9353</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Asuransi/Prudential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>455e7d81-faf1-4afa-aa30-fff717a3bd33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Briva/briva</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87a16fe1-1781-4e9a-af9e-ddf37a297618</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Brizzi/Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52578908-81d4-4681-a168-fe0cdffe6299</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Cicilan/OTO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7f58e8e-3060-4a45-936c-4ddcc5f1e0e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Cicilan/WOM</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f999bea-0489-4e7b-bd2b-570db28e16a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee83a275-ec33-4f22-8396-3f4fc6903f4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/Citibank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2e99424-d6fc-48d1-89e5-b631f03eccf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/DBS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c369187f-3fab-4969-9b77-a3da91867744</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/HSBC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>023f16e7-e1b1-4a2d-9525-87542da9e54d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Credit Card/Standard Chartered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4b9e471-5071-4181-bfb2-67082026b43b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/Dompet Dhuafa</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>772b0262-f6cd-4877-97a0-3f87affc12b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Donasi/YBM BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b04744c5-c237-4c4f-8d7f-5ad4a9be0487</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/DPLK/dplk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e99c470-a54a-4d62-8fd1-2d14c35b7b64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/KAI/KAI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>365c8294-3178-42ac-9526-1ab1254c577a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Kode QR/Kode QR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb48078e-bd5b-4d3a-af61-dc3bed904bd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Listrik/Tagihan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2aa7e7da-4a76-4919-9965-03492d8e44d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Listrik/Token</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>816ba2c4-318f-49c4-b60c-b29f23675aa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pascabayar/pascabayar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd8a49be-e509-4697-9de1-11ea43bf8689</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Artarjasa Mentari IM3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18af5709-9715-45eb-856e-4e5c534b6749</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae1dcec6-9d27-41c4-b67d-ee8cb11b8353</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Finnet Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91e0b8ff-4c1e-46e4-b7fa-bf2ac39cb53c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Mitrakom XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>267ddb8b-9d35-48db-b5dd-7ba255b2668f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7540fc9b-bdbd-4c8b-b84e-0bc423a3abd6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa/Three</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdcd83fc-84ae-4df4-8ad4-bf3fc7558758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa Data/Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9245ab63-b20d-431c-8dea-2776e62a61ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Pulsa Data/Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6638da3-2e4f-4100-ac58-bc26e760b0c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Tarik Tunai/Tarik tunai</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5370d52a-7d33-4c7e-92df-ce8c95721dcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Telkom/telkom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98728363-301f-4759-a20e-a4ba0685ab1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BCA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aaffdc34-6843-4719-88d6-eaef16119419</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BNI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>598017df-1bb7-4433-8150-8e17984f1e30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer BRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b00cac3-be25-4f8d-8844-4d431259f61d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer CITIBANK</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca260440-63c3-4e2f-9c08-87286961fb37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Transfer/Transfer MANDIRI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8b1befa-325d-4d7f-b742-6d53830fb15e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/indosat GIG</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e295b770-f845-4631-96ca-600cf0febfd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/My Republic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>103a899a-78db-4a15-a75f-e8d4a82612aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/TV/Transvision</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9acd9e9c-8a4c-4174-a644-10963185c351</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71232e50-84d4-40dc-892e-e08390c80f0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/GoPay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35ffab95-d34f-49d7-906d-c4eacfd8344e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0376ffd6-5dac-4baa-8816-af626ee3cf72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/Ovo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2ffda43-7b01-4be3-afc7-6fa9598f2727</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/Wallet/ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cd59cbc-7700-4cd6-98e7-65ce95d2fad7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/PFM/Add Catatan Pemasukan Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e9d18e4-be2d-44c8-828d-d7a28c2fd15b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 - Cucumber prod/PFM/Add Catatan Pengeluaran Baru</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
