package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object filePath
     
    /**
     * <p></p>
     */
    public static Object identifierApp
     
    /**
     * <p></p>
     */
    public static Object username
     
    /**
     * <p></p>
     */
    public static Object password
     
    /**
     * <p></p>
     */
    public static Object dbUrl
     
    /**
     * <p></p>
     */
    public static Object dbName
     
    /**
     * <p></p>
     */
    public static Object dbPort
     
    /**
     * <p></p>
     */
    public static Object dbUsername
     
    /**
     * <p></p>
     */
    public static Object dbPassword
     
    /**
     * <p></p>
     */
    public static Object timeoutShort
     
    /**
     * <p></p>
     */
    public static Object timeoutMiddle
     
    /**
     * <p></p>
     */
    public static Object destinationPhone
     
    /**
     * <p></p>
     */
    public static Object nominalPulsa
     
    /**
     * <p></p>
     */
    public static Object destinationBank
     
    /**
     * <p></p>
     */
    public static Object destinationAccount
     
    /**
     * <p></p>
     */
    public static Object nominalTransfer
     
    /**
     * <p></p>
     */
    public static Object detailTransfer
     
    /**
     * <p></p>
     */
    public static Object typeGopay
     
    /**
     * <p></p>
     */
    public static Object dbHistory2
     
    /**
     * <p></p>
     */
    public static Object dbHistory10
     
    /**
     * <p></p>
     */
    public static Object accountNumber
     
    /**
     * <p></p>
     */
    public static Object dateIndo
     
    /**
     * <p></p>
     */
    public static Object element
     
    /**
     * <p></p>
     */
    public static Object walletOption
     
    /**
     * <p></p>
     */
    public static Object deviceHeight
     
    /**
     * <p></p>
     */
    public static Object deviceWidth
     
    /**
     * <p></p>
     */
    public static Object rowNumber
     
    /**
     * <p></p>
     */
    public static Object bankAcc
     
    /**
     * <p></p>
     */
    public static Object PIN
     
    /**
     * <p></p>
     */
    public static Object reportStatus
     
    /**
     * <p></p>
     */
    public static Object currentTestCaseId
     
    /**
     * <p></p>
     */
    public static Object currentTestSuitesID
     
    /**
     * <p></p>
     */
    public static Object saldoAwal
     
    /**
     * <p></p>
     */
    public static Object saldoAkhir
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += TestCaseMain.getParsedValues(RunConfiguration.getOverridingParameters())
    
            filePath = selectedVariables['filePath']
            identifierApp = selectedVariables['identifierApp']
            username = selectedVariables['username']
            password = selectedVariables['password']
            dbUrl = selectedVariables['dbUrl']
            dbName = selectedVariables['dbName']
            dbPort = selectedVariables['dbPort']
            dbUsername = selectedVariables['dbUsername']
            dbPassword = selectedVariables['dbPassword']
            timeoutShort = selectedVariables['timeoutShort']
            timeoutMiddle = selectedVariables['timeoutMiddle']
            destinationPhone = selectedVariables['destinationPhone']
            nominalPulsa = selectedVariables['nominalPulsa']
            destinationBank = selectedVariables['destinationBank']
            destinationAccount = selectedVariables['destinationAccount']
            nominalTransfer = selectedVariables['nominalTransfer']
            detailTransfer = selectedVariables['detailTransfer']
            typeGopay = selectedVariables['typeGopay']
            dbHistory2 = selectedVariables['dbHistory2']
            dbHistory10 = selectedVariables['dbHistory10']
            accountNumber = selectedVariables['accountNumber']
            dateIndo = selectedVariables['dateIndo']
            element = selectedVariables['element']
            walletOption = selectedVariables['walletOption']
            deviceHeight = selectedVariables['deviceHeight']
            deviceWidth = selectedVariables['deviceWidth']
            rowNumber = selectedVariables['rowNumber']
            bankAcc = selectedVariables['bankAcc']
            PIN = selectedVariables['PIN']
            reportStatus = selectedVariables['reportStatus']
            currentTestCaseId = selectedVariables['currentTestCaseId']
            currentTestSuitesID = selectedVariables['currentTestSuitesID']
            saldoAwal = selectedVariables['saldoAwal']
            saldoAkhir = selectedVariables['saldoAkhir']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
