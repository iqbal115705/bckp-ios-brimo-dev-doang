package option
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.detroitlabs.katalonmobileutil.touch.Scroll as Scroll
import com.detroitlabs.katalonmobileutil.touch.Scroll.ScrollFactor as ScrollFactor
import com.detroitlabs.katalonmobileutil.testobject.Finder as Finder

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

class amount {
	/**
	 * Refresh browser
	 */

	@Keyword
	def clickOptionPulsa(String amount){
		switch (amount) {
			case '10000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp10.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/XCUIElementTypeStaticText - Rp10.000'), 0)

				break
			case '15000':
			//				while (Mobile.verifyElementExist(findTestObject('Object Repository/Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp15.000 (1)'), 0, FailureHandling.OPTIONAL) == false) {
			//					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
			//				}
				Mobile.comment('masuk 15k')

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp15.000 (1)'), 0)

				break
			case '20000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp20.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp20.000'), 0)

				break
			case '25000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp25.000 (1)'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.comment('masuk kondisi 25k')

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp25.000 (1)'), 0)

				break
			case '30000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp30.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp30.000'), 0)

				break
			case '40000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp40.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp40.000'), 0)

				break
			case '50000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp50.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.comment('kondisi 50k')

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp50.000 (1)'), 0)

				break
			case '75000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp75.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp75.000'), 0)

				break
			case '100000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp100.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp100.000'), 0)

				break
			case '150000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp150.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp150.000'), 0)

				break
			case '200000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp200.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp200.000'), 0)

				break
			case '300000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp300.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp300.000'), 0)

				break
			case '500000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp500.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp500.000'), 0)

				break
			case '1000000':
				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp1.000.000'), 0, FailureHandling.OPTIONAL) == false) {
					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
				}

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp1.000.000'), 0)

				break
			default:
			//				while (Mobile.verifyElementExist(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp25.000 (1)'), 0, FailureHandling.OPTIONAL) == false) {
			//					Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)
			//				}

				Mobile.comment('masuk kondisi default')

				Mobile.tap(findTestObject('Pulsa Form/get from prod/XCUIElementTypeStaticText - Rp25.000 (1)'), 0)
				break
		}
	}
}