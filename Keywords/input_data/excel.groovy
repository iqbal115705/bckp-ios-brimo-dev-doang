package input_data
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.keyword.excel.ExcelKeywords

class excel {

	@Keyword
	def importData(String files, String sheets) {

		String excelFile = 'Datasets/'+files+'.xlsx'

		def workBook = ExcelKeywords.getWorkbook(excelFile)

		def sheet1 = ExcelKeywords.getExcelSheet(workBook, sheets)

		String pinNumb = ExcelKeywords.getCellByAddress(sheet1, 'C2')

		String[] sect = pinNumb.split("\\.")

		String sect1 = sect[0]

		GlobalVariable.username = ExcelKeywords.getCellValueByAddress(sheet1, 'A2')

		GlobalVariable.password = ExcelKeywords.getCellValueByAddress(sheet1, 'B2')

		GlobalVariable.PIN = sect1

		if(sheets.equalsIgnoreCase('brizzi')) {

			String nomine = ExcelKeywords.getCellByAddress(sheet1, 'E2')

			String[] parts = nomine.split("\\.");

			String part1 = parts[0];

			GlobalVariable.nomorTujuan = ExcelKeywords.getCellByAddress(sheet1, 'D2')

			GlobalVariable.nominal = part1

			GlobalVariable.rekeningDigunakan = ExcelKeywords.getCellByAddress(sheet1, 'F2')
		} else if (sheets.equalsIgnoreCase('briva')) {
			GlobalVariable.paymentType = ExcelKeywords.getCellByAddress(sheet1, 'D2')

			GlobalVariable.nomorTujuan = ExcelKeywords.getCellByAddress(sheet1, 'E2')

			String nomine = ExcelKeywords.getCellByAddress(sheet1, 'F2')

			String[] parts = nomine.split("\\.");

			String part1 = parts[0];

			GlobalVariable.nominal = part1

			GlobalVariable.detail = ExcelKeywords.getCellByAddress(sheet1, 'G2')

			GlobalVariable.decision = ExcelKeywords.getCellByAddress(sheet1, 'H2')

			GlobalVariable.nameSave = ExcelKeywords.getCellByAddress(sheet1, 'I2')

			GlobalVariable.rekeningDigunakan = ExcelKeywords.getCellByAddress(sheet1, 'J2')
		}
	}
}