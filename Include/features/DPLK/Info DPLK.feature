Feature: DPLK

@Valid    
  Scenario Outline: User top up DPLK with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to see my DPLK account
    Then I already see my DPLK account

    Examples: 
      | username   | password   | pin 	 | dplkNumber	| amountDPLK	| decision	| name		| debit	| condition	|
      | denstoo02061997 | Jakarta123 | 123457 | 1040058		| 10000				| NULL			| NULL		| MUHAMAD TAUFAN	|	NULL			|
      
      
      
      