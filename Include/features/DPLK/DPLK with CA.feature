Feature: DPLK

  @Valid
  Scenario Outline: User top up DPLK with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to see my DPLK account
    #When I saw my top up DPLK history
    When I want to add recipient of DPLK
    When I try adding recipient of DPLK account
    When I inputting amount for the DPLK, then choose account
    When I confirm top up DPLK account
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username        | password   | pin    | dplkNumber | amountDPLK | decision | name | debit          | condition |
      #| denstoo02061997 | Jakarta123 | 123457 |    1040058 |      10000 | Save     | DPLK | MUHAMAD TAUFAN | NULL      |
      | case |
      |    1 |

  @dplkNoLogin
  Scenario Outline: User top up DPLK with CA
    Given Case <case>
    And I want to see my DPLK account
    #When I saw my top up DPLK history
    When I want to add recipient of DPLK
    When I try adding recipient of DPLK account
    When I inputting amount for the DPLK, then choose account
    When I confirm top up DPLK account
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username        | password   | pin    | dplkNumber | amountDPLK | decision | name | debit          | condition |
      #| denstoo02061997 | Jakarta123 | 123457 |    1040058 |      10000 | Save     | DPLK | MUHAMAD TAUFAN | NULL      |
      | case |
      |    1 |
