Feature: Pascabayar to XL
  I want to test feature Transfer product XL with automation

  @EndToEnd
  Scenario Outline: User pay pascabayar (XL) bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay pascabayar bill
    When I want to add recipient of pascabayar bill
    When I try adding recipient of pascabayar bill
    When I choose account and saving the number
    When I confirm payment of pascabayar bill
    When I validate my pin before transaction
    Then Check transaction success
    
    Examples: 
      | case |
      |    3 |

  @EndToEndNoLogin
   Scenario Outline: User pay pascabayar (XL) bill
    Given Case <case>
    And I also pay pascabayar bill
    When I want to add recipient of pascabayar bill
    When I try adding recipient of pascabayar bill
    When I choose account and saving the number
    When I confirm payment of pascabayar bill
    When I validate my pin before transaction
    Then Check transaction success
    
    Examples: 
      | case |
      |    3 |
