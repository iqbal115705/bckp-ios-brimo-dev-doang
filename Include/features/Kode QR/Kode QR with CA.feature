Feature: Kode QR

  @Valid
  Scenario Outline: User generate QR code
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want generate my own QR code
    When I choose account <debit> for money reception on my own QR code
    When I validate my pin with <pin> before transaction
    Then I see my own QR code and then <decision>

    Examples: 
      | username        | password   | pin    | decision | debit          |
      | denstoo02061997 | Jakarta123 | 123457 | NULL     | MUHAMAD TAUFAN |

  @prod
  Scenario Outline: User generate QR code
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I will generate my own QR code
    When I choose account for money reception on my own QR code
    When I validate my pin before transaction
    Then I see my QR code and then choose decision

    Examples: 
      #| username      | password    | pin    | decision | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | New      | giro iqbal |
      | case |
      |    1 |

  @prodNoLogin
  Scenario Outline: User generate QR code
    Given Case <case>
    And I will generate my own QR code
    When I choose account for money reception on my own QR code
    When I validate my pin before transaction
    Then I see my QR code and then choose decision

    Examples: 
      | case |
      |    1 |

  
   @FMQRCode
  Scenario Outline: User generate QR code from fast menu
    Given Case <case>
    Given I start application
    And I will generate my own QR code from fast menu
    When I choose account for money reception on my own QR code
    When I validate my pin before transaction
    Then I see my QR code and then choose decision

    Examples: 
      #| username      | password    | pin    | decision | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | New      | giro iqbal |
      | case |
      |    1 |