Feature: Transfer  to Mandiri
  I want to test feature Transfer product Mandiri with automation

  @EndToEnd
  Scenario Outline: User transfer money to Mandiri
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I will to transfer money
    When I saw my money transfer history
    When I want to add recipient of money transfer
    When I try adding recipient of money transfer
    When I inputting the transfer amount
    When I confirm transfer bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @EndToEndNoLogin
  Scenario Outline: User transfer money to Mandiri
    Given Case <case>
    And I will to transfer money
    When I saw my money transfer history
    When I want to add recipient of money transfer
    When I try adding recipient of money transfer
    When I inputting the transfer amount
    When I confirm transfer bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
