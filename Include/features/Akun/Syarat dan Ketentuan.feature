Feature: Title of your feature
  I want to use this template for my feature file
  @tag1
  Scenario Outline: Title of your scenario outline
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want use account feature
    And I want to know terms and conditions
    Then I got terms and conditions info
    Examples: 
      | username   | password   |
      | taufan123456 | Jakarta123 |