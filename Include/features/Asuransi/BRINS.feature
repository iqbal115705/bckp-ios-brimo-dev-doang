@tag
Feature: Asuransi BRINS
  I want to use this template for my feature file

  @BRINS
  Scenario Outline: User pay open briva bill with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username   | password   | pin    | condition | detail                   | insuranceType | paymentType   | insuranceNumber | amountInsurance | whatPayment | decision | name           | debit |
      #| bribri0002 | Jakarta123 | 123457 | NULL      | Prudential Premi Pertama | Prudential    | Premi Pertama |        00900043 |           25000 | Open        | Save     | PPremi Pertama | RATRI |
      | case |
      |    1 |

  @BRINSNoLogin
  Scenario Outline: User pay open briva bill with CA
    Given Case <case>
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  @BRINSForceClose1
  Scenario Outline: BRINS force close at Asuransi page
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    #1
    And I try force close app and confirm back login page

    Examples: 
      | case |
      |    1 |

  @BRINSForceClose2
  Scenario Outline: BRINS force close at bill page
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    #2
    And I try force close app and confirm back login page

    Examples: 
      | case |
      |    1 |

  @BRINSForceClose3
  Scenario Outline: BRINS force close at confirmation page
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    #3
    And I try force close app and confirm back login page

    Examples: 
      | case |
      |    1 |

  @BRINSForceClose4
  Scenario Outline: BRINS force close at PIN
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    #4
    And I try force close app and confirm back login page

    Examples: 
      | case |
      |    1 |
