Feature: personal finance management
  I want to use this Catatan Keuangan or PFM feature

	@AddPengeluaran
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    When I go to PFM from dashboard
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I choose pembayaran
    And I input pengeluaran catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of spending
    And I check and verify the spending is listed in laporan by category

    Examples: 
      | case |
      |    4 |

  @AddPengeluaranFM
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    Given I start application
    When I go to PFM from fast menu
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I choose pembayaran
    And I input pengeluaran catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of spending
    And I check and verify the spending is listed in laporan by category

    Examples: 
      | case |
      |    4 |

  #|    5 |
  #|    6 |
  
   @AddPengeluaranNoLogin
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    When I go to PFM from dashboard
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I choose pembayaran
    And I input pengeluaran catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of spending
    And I check and verify the spending is listed in laporan by category

    Examples: 
      | case |
      |    4 |
  
  @AddPemasukan
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    When I go to PFM from dashboard
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I input pemasukan catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of earning
    And I verify the earning is listed in laporan page

    Examples: 
      | case |
      |    1 |
  
  @AddPemasukanFM
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    Given I start application
    When I go to PFM from fast menu
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I input pemasukan catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of earning
    And I verify the earning is listed in laporan page

    Examples: 
      | case |
      |    1 |

  #|    2 |
  #|    3 |
  
   @AddPemasukanNoLogin
  Scenario Outline: add catatan pengeluaran baru
    Given Case <case>
    When I go to PFM from dashboard
    And I choose the catatan page
    When I go to tambah catatan
    And I input jumlah catatan
    And I try choose the date
    And I choose for catatan category
    And I input pemasukan catatan
    And I tap simpan button
    Then I verify perubahan catatan keuangan is listed
    When I verify the detail of earning
    And I verify the earning is listed in laporan page

    Examples: 
      | case |
      |    1 |
  
  @tag1
  Scenario Outline: verify spending, earning and report are empty (tc 101, 201 & 301)
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    Then I verify the <page> page there is no record yet

    Examples: 
      | page        |
      | pengeluaran |
      | pemasukan   |
      | laporan     |

  @tag1b
  Scenario Outline: verify spending, earning and report are empty (tc 101, 201 & 301)
    Given Case <case>
    Given I start application
    When I go to PFM from fast menu
    And I choose the catatan page
    Then I verify this page there is no record yet
    And I verify this range in laporan page

    Examples: 
      | case |
      |    7 |
      |    8 |
      |    9 |

  @tag2
  Scenario Outline: check jumlah in some condition
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I try input to verify <amount>

    Examples: 
      | page      | amount      |
      #|	pengeluaran	| 12345678910	|
      #|	pengeluaran	| 0000000000	|
      | pemasukan | 12345678910 |
      | pemasukan |  0000000000 |

  @tag3
  Scenario Outline: check tanggal in some condition
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I go to tambah catatan for <page>
    And I try choose <date> date

    Examples: 
      | page        | date      |
      | pengeluaran | today     |
      | pengeluaran | yesterday |
      | pengeluaran | tomorrow  |
      | pemasukan   | today     |
      | pemasukan   | yesterday |
      | pemasukan   | tomorrow  |

  @tag5
  Scenario Outline: edit catatan pengeluaran which have reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for a reference number
    And I try edit list
    And I edit category for <new_category>
    And I edit note for <page>
    And I tap simpan button

    Examples: 
      | page        | new_category |
      | pengeluaran | Uang Keluar  |

  @tag6
  Scenario Outline: delete catatan pengeluaran which have reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for a reference number
    And I try delete list for <page>

    Examples: 
      | page        |
      | pengeluaran |

  @tag7
  Scenario Outline: edit catatan pengeluaran which have no reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try edit list
    And I edit jumlah <amount>
    And I edit category for <new_category>
    And I try edit <date> date
    And I edit pembayaran for <payment>
    And I edit note for <page>
    And I tap simpan button

    Examples: 
      | page        | amount | date  | payment | new_category |
      | pengeluaran |  11000 | today | Tunai   | Uang Keluar  |

  @tag8
  Scenario Outline: delete catatan pengeluaran which have no reference number
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try delete list for <page>

    Examples: 
      | page        |
      | pengeluaran |

  @tag10
  Scenario Outline: edit catatan pemasukan
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try edit list
    And I edit jumlah <amount>
    And I edit category for <new_category>
    And I try edit <date> date
    And I edit note for <page>
    And I tap simpan button

    Examples: 
      | page      | amount | date  | new_category |
      | pemasukan |  11000 | today | Uang Masuk   |

  @tag11
  Scenario Outline: delete catatan pemasukan
    Given I start application
    When I go to PFM from fast menu
    And I go to <page> page
    When I check the list look for have no reference number
    And I try delete list for <page>

    Examples: 
      | page      |
      | pemasukan |
