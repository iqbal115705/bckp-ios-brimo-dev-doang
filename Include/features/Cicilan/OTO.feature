Feature: Cicilan

 @prodXLSOTO
  Scenario Outline: User pay installment with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay the installment
    When I want to add payment of installment
    When I write installment code
    When I saw amount of installment payment
    When I confirm of installment payment
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
      
  @prodXLSOTONoLogin
  Scenario Outline: User pay installment with CA
    Given Case <case>
    And I also pay the installment
    When I want to add payment of installment
    When I write installment code
    When I saw amount of installment payment
    When I confirm of installment payment
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |