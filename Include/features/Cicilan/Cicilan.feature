Feature: Cicilan

  @Valid
  Scenario Outline: User pay installment with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want pay installment with <condition> for <username>
    When I saw my installment payment history
    When I want to add payment of installment
    When I write installment code in condition with <installmentType> and <installmentNumber>
    When I saw amount of installment payment and <decision> with <name> , then choose account <debit>
    When I confirm of installment payment
    When I validate my pin with <pin> before transaction
    Then Transaction success

    Examples: 
      | username        | password   | pin    | installmentType | installmentNumber | decision | name | debit          | condition |
      | denstoo02061997 | Jakarta123 | 123457 | OTO             |      000126084750 | Save     | OTO  | MUHAMAD TAUFAN | NULL      |

  @prodOTO
  Scenario Outline: User pay installment with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I also pay the installment
    When I want to add payment of installment
    When I write installment code in condition with <installmentType> and <installmentNumber>
    When I saw amount of installment payment and <decision> with <name> , then choose account <debit>
    When I confirm of installment payment

    #When I validate my pin with <pin> before transaction
    #Then Transaction success
    Examples: 
      | username      | password   | pin    | installmentType | installmentNumber | decision | name | debit          |
      | iqbalsugandhi | Ragunan123 | 123457 | OTO             |      101011801787 | null     | null | @sugandhiiqbal |

  @prodWOM
  Scenario Outline: User pay installment with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I also pay the installment
    When I want to add payment of installment
    When I write installment code in condition with <installmentType> and <installmentNumber>
    When I saw amount of installment payment and <decision> with <name> , then choose account <debit>
    When I confirm of installment payment

    #When I validate my pin with <pin> before transaction
    #Then Transaction success
    Examples: 
      | username      | password   | pin    | installmentType | installmentNumber | decision | name | debit          |
      | iqbalsugandhi | Ragunan123 | 123457 | WOM             |      000126084750 | null     | null | @sugandhiiqbal |

  @prodXLS
  Scenario Outline: User pay installment with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay the installment
    When I want to add payment of installment
    When I write installment code
    When I saw amount of installment payment
    When I confirm of installment payment
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
      |    2 |

  #| username      | password   | pin    | installmentType | installmentNumber | decision | name | debit          |
  #| iqbalsugandhi | Ragunan123 | 123457 | WOM             |      000126084750 | null     | null | @sugandhiiqbal |
 