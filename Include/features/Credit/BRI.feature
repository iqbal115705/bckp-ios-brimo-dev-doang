Feature: Credit BRI

  Scenario Outline: User pay credit bill
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to pay credit bill
    When I saw payment of credit bills history
    When I want to add recipient of credit bill
    When I try adding recipient of credit bill condition with <creditBank> and <creditNumber>
    When I inputting <totalInput> and <amount> for the credit amount
    When I confirm payment of credit bill with <detail>
    When I validate my pin with <pin> before transaction
    Then Transaction success

    Examples: 
      | username   | password   | pin    | creditBank | creditNumber     | totalInput | amount | detail       |
      | bribri0001 | Jakarta123 | 123457 | BRI        | 4365020100000107 | Manual     |  10000 | kartu kredit |
      | bribri0001 | Jakarta123 | 123457 | BRI        | 4365020100000107 | Penuh      | Null   | kartu kredit |
      | bribri0001 | Jakarta123 | 123457 | BRI        | 4365020100000107 | Minimal    | Null   | kartu kredit |

  @prodBRI
  Scenario Outline: User pay credit bill BRI
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I suppose to pay credit bill
    When I want to add recipient of credit bill
    When I try adding recipient bank and number of credit bill condition
    When I input total and amount for the credit amount and decision with name , then choose account
    When I confirm payment of credit bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  @prodBRINoLogin
  Scenario Outline: User pay credit bill BRI
    Given Case <case>
    And I suppose to pay credit bill
    When I want to add recipient of credit bill
    When I try adding recipient bank and number of credit bill condition
    When I input total and amount for the credit amount and decision with name , then choose account
    When I confirm payment of credit bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
