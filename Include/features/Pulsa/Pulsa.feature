@tag
Feature: Pulsa

  @ViaFastMenu
  Scenario Outline: User buy pulsa (Mentari) with CA via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
      |    2 |
      |    3 |
      |    4 |
      |    5 |
      |    6 |
      #|    7 |
      #|    8 |

  #
  @prodMentari
  Scenario Outline: User buy pulsa (Mentari) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  #
  #|    2 |
  #|    3 |
  #|    4 |
  #|    5 |
  #|    6 |
  @prodMentariNoLogin
  Scenario Outline: User buy pulsa (Mentari) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  @prodSimpati
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  #
  @prodSimpatiNoLogin
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @prodXL
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |

  #
  @prodXLNoLogin
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |

  @prod3
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    4 |

  @prod3NoLogin
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    4 |

  #
  @prodSmartFren
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    5 |

  @prodSmartFrenNoLogin
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    5 |

  #
  @prodAxis
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    6 |

  @prodAxisNoLogin
  Scenario Outline: User buy pulsa (Simpati) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    6 |
      
  @ViaFMIndosat
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
      
   @ViaFMTelkomsel
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
      
  @ViaFMXL
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |
      
  @ViaFMThree
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    4 |
      
   @ViaFMSmartfren
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    5 |   
      
  @ViaFMAxis
  Scenario Outline: User buy pulsa (Indosat) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    6 |     
       
   #@Valid
   #Scenario Outline: User buy pulsa (XL)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa	|
      #| bribri0001 | Jakarta123 | 123457 | 081911106755	| 25000		| NEW				|	RATRI					| XL		|
        #
   #@Valid
   #Scenario Outline: User buy pulsa (XL)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa	|
      #| bribri0001 | Jakarta123 | 123457 | 081911106755	| 50000		| NEW				|	RATRI					| XL		|
      #| bribri0001 | Jakarta123 | 123457 | 081911106755	| 100000	| NEW				|	RATRI					| XL		|
      #| bribri0001 | Jakarta123 | 123457 | 081911106755	| 200000	| NEW				|	RATRI					| XL		|
      #
    #@Valid
    #Scenario Outline: User buy pulsa (Three)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 20000		| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 50000		| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 75000		| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 100000	| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 150000	| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 300000	| NEW				|	RATRI					|	Three	|
      #| bribri0001 | Jakarta123 | 123457 | 089785452541	| 500000	| NEW				|	RATRI					|	Three	|
      #
    #@Valid
    #Scenario Outline: User buy pulsa (Axis)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa	|
      #| bribri0001 | Jakarta123 | 123457 | 083811109000	| 25000		| NEW				|	RATRI					| Axis	|
      #| bribri0001 | Jakarta123 | 123457 | 083811109000	| 50000		| NEW				|	RATRI					| Axis	|
      #| bribri0001 | Jakarta123 | 123457 | 083811109000	| 100000	| NEW				|	RATRI					| Axis	|
      #| bribri0001 | Jakarta123 | 123457 | 083811109000	| 200000	| NEW				|	RATRI					| Axis	|
      #
    #@Valid
    #Scenario Outline: User buy pulsa (Smartfren)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa			|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 20000		| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 25000		| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 50000		| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 100000	| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 150000	| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 200000	| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 300000	| NEW				|	RATRI					| Smartfren	|
      #| bribri0001 | Jakarta123 | 123457 | 08880000034	| 500000	| NEW				|	RATRI					| Smartfren	|
      #
   #@Valid
   #Scenario Outline: User buy pulsa (IM3)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa	|
      #| bribri0001 | Jakarta123 | 123457 | 08561234567	| 25000		| NEW				|	RATRI					| IM3		|
      #| bribri0001 | Jakarta123 | 123457 | 08561234567	| 50000		| NEW				|	RATRI					| IM3		|
      #| bribri0001 | Jakarta123 | 123457 | 08561234567	| 100000	| NEW				|	RATRI					| IM3		|
      #| bribri0001 | Jakarta123 | 123457 | 08561234567	| 150000	| NEW				|	RATRI					| IM3		|
      #
    #@Valid
    #Scenario Outline: User buy pulsa (Mentari)
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I want to buy pulsa with <condition> for <username>
    #When I write my <numberPhone> phone and <amount>
    #When I choose account <debit> for pulsa
    #When I confirm pulsa bill cause <pulsa>
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
#
    #Examples: 
      #| username   | password   | pin 	 | numberPhone				| amount	| condition		| debit 				| pulsa		|
      #| bribri0001 | Jakarta123 | 123457 | 081512341234	| 25000		| NEW				|	RATRI					| Mentari	|
      #| bribri0001 | Jakarta123 | 123457 | 081512341234	| 50000		| NEW				|	RATRI					| Mentari	|
      #| bribri0001 | Jakarta123 | 123457 | 081512341234	| 100000	| NEW				|	RATRI					| Mentari	|
      #| bribri0001 | Jakarta123 | 123457 | 081512341234	| 150000	| NEW				|	RATRI					| Mentari	|
