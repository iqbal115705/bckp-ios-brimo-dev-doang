Feature: Paket Data (Indosat)
  I want to test feature Pulsa product Indosat with automation

  @EndToEnd
  Scenario Outline: User buy data package (Indosat) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |

  @EndToEndNoLogin
  Scenario Outline: User buy data package (Indosat) with CA
    Given Case <case>
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |

  @EndToEndViaFastMenu
  Scenario Outline: User buy data package (Indosat) via fast menu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |
