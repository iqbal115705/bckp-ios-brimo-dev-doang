Feature: Pulsa (Telkomsel)
  I want to test feature Pulsa product Telkomsel with automation

  @EndToEnd
  Scenario Outline: User buy pulsa (Telkomsel) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @EndToEndNoLogin
  Scenario Outline: User buy pulsa (Telkomsel) with CA
    Given Case <case>
    And I buy pulsa or data
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @EndToEndViaFastMenu
  Scenario Outline: User buy pulsa (Telkomsel) via fastmenu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I inputting my phone number and pulsa amount
    When I choose account for top up pulsa
    When I confirm pulsa bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
