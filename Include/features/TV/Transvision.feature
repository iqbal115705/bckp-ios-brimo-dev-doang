Feature: Transvision TV
  I want to test feature TV product Transvision with automation

  @EndToEnd
  Scenario Outline: User pay tv with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I have to pay tv bill
    When I want to add payment of tv
    When I write tv code in condition
    When I saw amount of tv payment and choose account
    When I confirm of tv payment
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @EndToEndNoLogin
  Scenario Outline: User pay indosat tv with CA
    Given Case <case>
    And I have to pay tv bill
    When I want to add payment of tv
    When I write tv code in condition
    When I saw amount of tv payment and choose account
    When I confirm of tv payment
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
