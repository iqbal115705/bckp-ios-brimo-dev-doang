Feature: Indosat TV
  I want to test feature TV product Indosat with automation

  @EndToEnd
  Scenario Outline: User pay tagihan electricity bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  @EndToEndNoLogin
  Scenario Outline: User pay tagihan electricity bill
    Given Case <case>
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
