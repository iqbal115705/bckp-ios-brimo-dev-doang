Feature: Listrik

  @Valid
  Scenario Outline: User pay electricity bill with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want pay electricity with <condition> for <username>
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I write payment bill of electricity in condition with <listrikType> and <listrikNumber>
    When I inputting <amountListrik> for bill of electricity and <decision> with <name> , then choose account <debit>
    When I confirm electricity transaction with <listrikType>
    When I validate my pin with <pin> before transaction
    Then Transaction success

    Examples: 
      | username        | password   | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit          | condition |
      #| denstoo02061997 | Jakarta123 | 123457 | Token				| 32137592468			| 20000					| listrik			| NULL			| NULL			| MUHAMAD TAUFAN	|	NEW				|
      | denstoo02061997 | Jakarta123 | 123457 | Tagihan     |  535811629752 |         20000 | listrik | NULL     | NULL | MUHAMAD TAUFAN | NEW       |

  @prodXLS
  Scenario Outline: User pay electricity bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    1 |
      |    2 |

  @prodXLSToken
  Scenario Outline: User pay electricity bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    1 |

  @prodXLSTokenNoLogin
  Scenario Outline: User pay electricity bill
    Given Case <case>
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    1 |

  @prodXLSTagihan
  Scenario Outline: User pay electricity bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    2 |

  @prodXLSTagihanNoLogin
  Scenario Outline: User pay electricity bill
    Given Case <case>
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    2 |

  @prodPrepaid
  Scenario Outline: User pay postpaid electricity bill with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I write payment bill of electricity in condition with <listrikType> and <listrikNumber>
    When I inputting <amountListrik> for bill of electricity and <decision> with <name> , then choose account <debit>
    When I confirm electricity transaction with <listrikType>
    When I validate my pin with <pin> before transaction
    Then Check transaction success

    Examples: 
      | username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      | iqbalsugandhi | Jangkrik511 | 112233 | Token       |  227440481596 |         20000 | listrik | NULL     | NULL | giro iqbal |
