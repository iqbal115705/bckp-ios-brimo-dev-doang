@tag
Feature: Tarik Tunai

  @Valid
  Scenario Outline: User withdrawing of cash online
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to cash withdrawal without card
    When I choose <amountWithdraw> for withdrawal, then choose account <debit>
    When I confirm of cash withdrawal

    #When I validate my pin with <pin> before transaction
    #Then Withdrawal success
    Examples: 
      | username | password | pin | debit | amountWithdraw |

  #| taufan123456 | Jakarta123 | 123457 	| RATRI	| 100000					|
  #| brimosv004 | Jakarta123 | 123457 	| RATRI	| 100000					|
  @prod
  Scenario Outline: User withdrawing of cash online
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to cash withdrawal without card
    When I choose amount withdraw and choose account debit
    When I confirm of cash withdrawal
    When I validate my pin before transaction
    Then Withdrawal success
    Examples: 
      | case |
      |    1 |
      
  @prodNoLogin
  Scenario Outline: User withdrawing of cash online
    Given Case <case>
    And I want to cash withdrawal without card
    When I choose amount withdraw and choose account debit
    When I confirm of cash withdrawal
    When I validate my pin before transaction
    Then Withdrawal success
    Examples: 
      | case |
      |    1 |
